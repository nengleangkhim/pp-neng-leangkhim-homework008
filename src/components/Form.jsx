import React from 'react'
import { Form, Group, Label, Control, Text, Button, Col, Container,Row,InputGroup,  } from 'react-bootstrap';

export default function Formm() {
    return (
        <div>
            <Container>
                <br />
                    <h2>Person Info</h2>
                <br />
                <Row>
                    <Col>
                        <Form>
                            <Form.Group as={Col} controlId="validationCustomUsername">
                                <Form.Label>Username</Form.Label>
                                <InputGroup hasValidation>
                                    <InputGroup.Prepend>                                
                                    </InputGroup.Prepend>
                                    <Form.Control
                                        type="text"
                                        placeholder="Username"
                                        aria-describedby="inputGroupPrepend"
                                        required
                                    />
                                    <Form.Control.Feedback type="invalid">
                                        Please choose a username.
                                    </Form.Control.Feedback>
                                </InputGroup>
                            </Form.Group>
                            <Form.Group as={Col}  controlId="validationFormikUsername">
                                <Form.Label>Email</Form.Label>
                                <InputGroup hasValidation>                                
                                    <Form.Control
                                        type="email"
                                        placeholder="email"                    
                                        name="email"
                                        // value={values.email}
                                        // onChange={handleChange}
                                        // isInvalid={!!errors.email}
                                    />
                                    <Form.Control.Feedback type="invalid">
                                        {/* {errors.email} */}
                                    </Form.Control.Feedback>
                                </InputGroup>
                            </Form.Group>
                
                            <Button variant="primary" type="submit" className="mgl">
                                Save
                            </Button>
                            <Button variant="light" type="reset"  className="ml-2">
                                Cancel
                            </Button>
                        </Form>
                    </Col>
                    <Col>
                    <fieldset className="ml-5">
                        <Form.Group as={Row}>
                            <Form.Label as="legend" column sm={12}>
                                <h5>Gender</h5>
                            </Form.Label>
                            <Col sm={2} className="">                                
                                <Form.Check
                                    type="radio"
                                    label="Male"
                                    name="formHorizontalRadios"
                                    id="formHorizontalRadios1"
                                />
                                                        
                            </Col>
                            <Col sm={2} >
                                <Form.Check
                                    type="radio"
                                    label="Female"
                                    name="formHorizontalRadios"
                                    id="formHorizontalRadios1"
                                />
                                                        
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row}>
                            <Form.Label as="legend" column sm={12}>
                                <h5>Job</h5>
                            </Form.Label>
                            <Col sm={3} className="">
                                <Form.Check
                                    type="checkbox"
                                    label="Student"
                                    name="formHorizontalCheck"
                                    id="formHorizontalCheck1"
                                />
                                                        
                            </Col>
                            <Col sm={3} >
                                <Form.Check
                                    type="checkbox"
                                    label="Teacher"
                                    name="formHorizontalCheck"
                                    id="formHorizontalCheck1"
                                />
                                                        
                            </Col>
                            <Col sm={3} >
                                <Form.Check
                                    type="checkbox"
                                    label="Developer"
                                    name="formHorizontalCheck"
                                    id="formHorizontalCheck1"
                                />
                                                        
                            </Col>
                        </Form.Group>
                    </fieldset>
                    
                    </Col>
                </Row>
                
            </Container>
            
        </div>
    )
}
