import React from 'react'
import { Navbar, NavbarBrand } from 'react-bootstrap';

export default function Navbarr() {
    return (
        <div>
            <Navbar bg="dark" variant="dark" >
                <Navbar.Brand href="#home" className="ml">
                <img
                    alt=""
                    src="/logo192.png"
                    width="30"
                    height="30"
                    className="d-inline-block align-top"
                />{' '}
                Person Into React Bootstrap 
                </Navbar.Brand>
            </Navbar>
        </div>
    )
}
