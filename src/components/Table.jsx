import React from 'react'
import { Nav, Item, Link, Table, Card,Body, Header, Title, Text, Container,Button,Tabs, Tab, Dropdown,DropdownButton, ButtonGroup, Footer } from 'react-bootstrap';
import { FaEye, FaEdit, FaTrash } from 'react-icons/fa';

export default function Tablee() {
    
    return (
        <div>
            <Container>
                <h3>Display Data As :</h3>
                <br/>
                <Tabs defaultActiveKey="Table" id="uncontrolled-tab-example">
                    <Tab eventKey="Table" title="Table">
                        <Table striped bordered hover className="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Gender</th>
                                    <th>Email</th>
                                    <th>Job</th>
                                    <th>Create At</th>
                                    <th>Update At</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <Button variant="primary"> <FaEye /> View</Button>{' '}
                                        <Button variant="info"><FaEdit/> Update</Button>{' '}
                                        <Button variant="danger"><FaTrash/> Delete</Button>
                                    </td>
                                </tr>                                
                            </tbody>
                        </Table>
                        
                    </Tab>
                    <Tab eventKey="Card" title="Card">                     
                        <Card border="primary" style={{ width: '15rem' }}>
                            <Card.Header>                       
                                <DropdownButton as={ButtonGroup} variant="success" title="Action" id="bg-nested-dropdown" className="ml-5 btn-action">
                                    <Dropdown.Item eventKey="1"><FaEye /> View</Dropdown.Item>
                                    <Dropdown.Item eventKey="2"><FaEdit/> Update</Dropdown.Item>
                                    <Dropdown.Item eventKey="3"><FaTrash/> Delete</Dropdown.Item>
                                </DropdownButton>
                            </Card.Header>
                            <Card.Body>
                                <Card.Title>Primary Card Title</Card.Title>
                                <Card.Text>
                                    Some quick example text to build on the card title 
                                </Card.Text>
                            </Card.Body>
                            <Card.Footer>Footer</Card.Footer>
                        </Card>
                    </Tab>                    
                </Tabs>               
            </Container>
        </div>
    )
}
